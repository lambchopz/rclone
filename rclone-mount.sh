#!/bin/bash
# Mounts rclone acd-primary-crypt drive
# crontab -e (pase the line below)
# */5 * * * * /home/alvin/Scipts/rclone/rclone-mount.sh
# Make script executable with: chmod a+x [script]

LOGFILE="/home/Logs/rclone-mount.log"
REMOTE="acd-primary-crypt:"
MOUNTPOINT="/home/alvin/acd-primary-mount"
TMUXSESSION="rclone-mount"

#ensures the log and mount directory exists
mkdir -p "$LOGFILE"
mkdir -p "$MOUNTPOINT"

#creates TMUX session and detachs for the rclone mount if it doesn't exist
tmux new -d -s "$TMUXSESSION"

#Ensures the script isn't already executing, exits if so
# "pidof -o %PPID" finds parent process of the pidof program, "-x" also includes process id's of shells running the named scripts, "$0" returns the name of the current sh script. 
# tee copies standard output to file, "-a" appends
if pidof -o %PPID -x "$0"; then
	echo "$(date "+%d.%m.%Y %T") EXIT: Already running." | tee -a "$LOGFILE"
	exit 1
fi

#/usr/bin/rclone mount --allow-non-empty --allow-other --max-read-ahead=2G --acd-templink-threshold=0 --contimeout=15s --checkers=16 --bwlimit=0 --retries=3 --timeout=30s --low-level-retries=1 --transfers=8 --dir-cache-time=30m acd-primary-crypt: /home/alvin/acd-primary-mount

exit

#unmount drive
#fusermount -u /home/alvin/acd-primary-mount